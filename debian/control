Source: python-formencode
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 python-all (>= 2.6.6-3),
 python-dnspython <!nocheck>,
 python-nose <!nocheck>,
 python-setuptools (>= 0.6a9),
 python3-all,
 python3-dnspython <!nocheck>,
 python3-pycountry <!nocheck>,
 python3-nose <!nocheck>,
 python3-setuptools,
Maintainer: Fabio Tranchitella <kobold@debian.org>
Uploaders:
 Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>,
 Piotr Ożarowski <piotr@debian.org>,
 Neil Muller <drnlmuller+debian@gmail.com>,
 Chris Lamb <lamby@debian.org>,
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/python-team/modules/python-formencode.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-formencode
Homepage: http://formencode.org
Rules-Requires-Root: no

Package: python-formencode
Architecture: all
Depends:
 python-dnspython,
 python-pkg-resources,
 ${misc:Depends},
 ${python:Depends},
Suggests:
 python-egenix-mxdatetime,
Description: validation and form generation Python package
 FormEncode is a validation and form generation package. The validation can
 be used separately from the form generation. The validation works on compound
 data structures, with all parts being nestable. It is separate from HTTP or
 any other input mechanism.
 .
 This package contains the Python 2 version of FormEncode.

Package: python3-formencode
Architecture: all
Depends:
 python3-dnspython,
 python3-pkg-resources,
 ${misc:Depends},
 ${python3:Depends},
Description: validation and form generation Python 3 package
 FormEncode is a validation and form generation package. The validation can
 be used separately from the form generation. The validation works on compound
 data structures, with all parts being nestable. It is separate from HTTP or
 any other input mechanism.
 .
 This package contains the Python 3 version of FormEncode.
